# UNI

A simply non graphical installation for Arch, Artix and Void, with a verification and elimination of those packages that may block the installation process for an unnecessary package that you can install later 

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Tarch1/uni/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Installation
**Wifi-setup**  
ip a  
rfkill unblock all  
ip link interface up  
connmanctl  
    `scan wifi`  
    `services`  
    `agent on`  
    `connect wifi_***_psk`  
    `quit`  
git clone https://github.com/Tarch1/Arch-tix  
cd Arch-tix/  
Adjust packages at the end of base_install  
bash base_install

## First setup
Use nmtui to connect on your network, then personalize your environment
bash full_setup  
Reboot and Enjoy!

##Troubleshooting

In case of error with gdm use Ctrl+Alt+F2

If gnome on wayland not start's on machine's with hybrid gpu setup comment 
     
    sed -i 's/DRIVER=="nvidia"/#DRIVER=="nvidia"/g' /lib/udev/rules.d/61-gdm.rules

PIPEWIRE TROUBLESHOOTING

If /etc/pipewire/ folder and its contents doesn't exist run

    cp -r /usr/share/pipewire /etc/

    than be sure /home/!!!your username!!!/.config/autostart/pipewire.desktop exist in case copy it :

    cp ~/UNI/Conf_files/Pipewire/pipewire.desktop ~/.config/autostart/
 
    if you have installed pipewire-media-session comment out at the Exec inside ~/.config/autostart/pipewire.desktop
 
    ###& /usr/bin/pipewire-media-session

and uncomment these 2 lines at the end of /etc/pipewire/pipewire.conf
    
    #{ path = "/usr/bin/pipewire" args = "-c pipewire-pulse.conf" }
    
    - if you have installed pipewire-media-session
 
       #{ path = "/usr/bin/pipewire-media-session" args = "" }

    - else if you have installed wireplumber replace the above command with
    
       { path = "wireplumber"  args = "" }

At the very end if either else not worked in /etc/pulse/client.conf change from yes to no

    autospawn = yes
    ;autospawn = yes

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## License
For open source projects, say how it is licensed.
